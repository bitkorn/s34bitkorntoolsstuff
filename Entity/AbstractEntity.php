<?php

namespace Bitkorn\ToolsStuff\Entity;

/**
 * All Entities extends AbstractEntity
 * Entities manage the relation from Data-Keys in Frontend to Data-Keys in database.
 *
 * @author allapow
 */
class AbstractEntity
{

    /**
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [];
    protected $isFlippedMapping = false;
    public $storage = [];
    public $storageOutside = [];

    /**
     * Flip if data comes from DB
     */
    public function flipMapping(bool $flipOnlyIfNotFlipped = true)
    {
        if ($flipOnlyIfNotFlipped && $this->isFlippedMapping) {
            return;
        }
        $this->mapping = array_flip($this->mapping);
        if ($this->isFlippedMapping) {
            $this->isFlippedMapping = false;
        } else {
            $this->isFlippedMapping = true;
        }
    }

    public function flipMappingBack()
    {
        if ($this->isFlippedMapping) {
            $this->mapping = array_flip($this->mapping);
            $this->isFlippedMapping = false;
        }
    }

    /**
     * Exchange $data to the $this->storage Array.
     * Flip Mapping first if data comes from database.
     * 
     * @param array $data
     * @return bool True on success or false if $this->storage empty.
     */
    public function exchangeArray(array $data) : bool
    {
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                $this->storage[$this->mapping[$key]] = $this->$key = $value;
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        return true;
    }

    /**
     * Exchange $data to an Array.
     * @param array $data
     * @return bool True on success or false if $this->storageOutside empty.
     */
    public function exchangeOutsiteArray($data): bool
    {
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                $this->storageOutside[$this->mapping[$key]] = $value;
            }
        }
        if (empty($this->storageOutside)) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @return array Storage keys wich have unequal values.
     * @throws \RuntimeException
     */
    public function compareStorageValues()
    {
        if (!isset($this->storage) || !isset($this->storageOutside)) {
            throw new \RuntimeException('One of the storage is empty. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        if (!is_array($this->storage) || !is_array($this->storageOutside)) {
            throw new \RuntimeException('One of the storage is not an Array. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        $storageKeys = array_values($this->mapping);
        $unequals = [];
        foreach ($storageKeys as $storageKey) {
            if (isset($this->storageOutside[$storageKey]) && $this->storage[$storageKey] != $this->storageOutside[$storageKey]) {
                $unequals[] = $storageKey;
            }
        }
        return $unequals;
    }

    /**
     * 
     * @return array
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    /**
     * 
     * @return bool
     */
    public function isMappingFlipped(): bool
    {
        return $this->isFlippedMapping;
    }

}
